pragma solidity ^0.4.22;

contract Greeter {
  string greeting;

  constructor(string _greeting) public {
    greeting = _greeting;
  }

  function greet() constant public returns (string) {
      return greeting;
  }
}
