import getopt, sys
from graph_tool.all import *
from random import randrange
from itertools import islice

def nth(iterable, n):
  return next(islice(iterable, n, None))

class Network:
  ACTIVE_NODE = [0.11,0.89,0.89,1]
  INACTIVE_NODE = [0.98,0.71,0.02,1]
  ACTIVE_CONNECTION = [0.85,0.85,0.85,1]

  def __init__(self, nodes, connections, out_size):
    # init random graph
    self.graph = Graph(directed=False)
    self.out_size = out_size
    self.graph.add_vertex(nodes)
    for _ in range(connections):
      s = randrange(self.graph.num_vertices())
      t = randrange(self.graph.num_vertices())
      if s != t and self.graph.edge(s, t) is None:
        self.graph.add_edge(
          self.graph.vertex(s),
          self.graph.vertex(t)
        )
    # remove unconnected graph
    for v in self.graph.vertices():
      if len(self.graph.get_out_edges(v))==0 and len(self.graph.get_in_edges(v))==0:
        try:
          self.graph.remove_vertex(v)
        except:
          pass
    # init graph view
    self.nodes = self.graph.new_vertex_property("bool", val=True)
    self.connections = self.graph.new_edge_property("bool", val=True)
    # drawing attributes
    self.vertex_shape = self.graph.new_vertex_property("string", val="triangle")
    self.vertex_color = self.graph.new_vertex_property("vector<float>", val=Network.ACTIVE_NODE)
    self.vertex_pen=self.graph.new_vertex_property("float", val=0)
    self.vertex_size=self.graph.new_vertex_property("int", val=12)
    self.edge_pen=self.graph.new_edge_property("float", val=0.6)
    self.edge_color=self.graph.new_edge_property("vector<float>", val=Network.ACTIVE_CONNECTION)

  def draw(self, output, pos=None):
    return graph_draw(
      GraphView(self.graph, vfilt=self.nodes, efilt=self.connections),
      pos=pos,
      output_size=(self.out_size,self.out_size),
      output=output,
      vertex_shape = self.vertex_shape,
      vertex_fill_color=self.vertex_color,
      vertex_pen_width=self.vertex_pen,
      vertex_size=self.vertex_size,
      edge_pen_width=self.edge_pen,
      edge_color=self.edge_color
    )

  def remove_node(self):
    rand_vertex = nth(self.graph.vertices(), randrange(self.graph.num_vertices()))
    self.nodes[rand_vertex] = False

  def add_node(self):
    disabled = [i for i in range(0, len(self.nodes.a)) if not self.nodes.a[i]]
    if len(disabled) > 0:
      enable = disabled[randrange(len(disabled))]
      self.nodes[enable] = True

  def remove_connection(self):
    rand_edge = nth(self.graph.edges(), randrange(self.graph.num_edges()))
    self.connections[rand_edge] = False

  def add_connection(self):
    disabled = [i for i in range(0, len(self.connections.a)) if not self.connections.a[i]]
    if len(disabled) > 0:
      enable = nth(self.graph.edges(), disabled[randrange(len(disabled))])
      self.connections[enable] = True

  def disable_node(self):
    rand_vertex = nth(self.graph.vertices(), randrange(self.graph.num_vertices()))
    for e in self.graph.get_out_edges(rand_vertex):
      self.connections[self.graph.edge(e[0], e[1])] = False
    self.vertex_color[rand_vertex] = Network.INACTIVE_NODE

  def enable_node(self):
    disabled = [i for i in range(0, len(self.nodes.a)) if self.vertex_color[i] == Network.INACTIVE_NODE]
    if len(disabled) > 0:
      enable = disabled[randrange(len(disabled))]
      self.vertex_color[enable] = Network.ACTIVE_NODE
      for e in self.graph.get_out_edges(enable):
        self.connections[self.graph.edge(e[0], e[1])] = True

# special effect: sparkle
SAFRON = [1,0.68,0,1]
EMERALD = [0,1,0.68,1]
PURPLE = [0.68,0.32,0.55,1]
def sparkle(net, pos, col):
    nodes = [nth(net.graph.vertices(), randrange(net.graph.num_vertices()))]
    oedges = []
    visited = []
    for _ in range(6):
      nnodes = []
      for e in oedges:
        net.edge_color[e] = Network.ACTIVE_CONNECTION
      while len(nodes) > 0:
        n = nodes.pop()
        edges = [f for f in net.graph.get_out_edges(n) if f[2] not in visited]
        for e in edges:
          net.edge_color[e] = col
          visited.append(e[2])
        oedges.extend(edges)
        nnodes.extend(net.graph.get_out_neighbors(n))
      next_frame(net, pos)
      next_frame(net, pos)
      next_frame(net, pos)
      nodes = nnodes
    for e in oedges:
      net.edge_color[e] = Network.ACTIVE_CONNECTION
    next_frame(net, pos)

# frames helper
frame_id = 1
def next_frame(g, p):
  global frame_id
  pos = g.draw('netsim_{0:04d}.png'.format(frame_id), p)
  frame_id += 1
  return pos

# parse cli options
def usage():
  print("usage: python3 netsim.py OPTIONS")
  print("where options is:")
  print("    -h, --help                 : display this message")
  print("    -s SIZE, --size SIZE       : the size of the output frame in pixels")
  print("    -f FRAMES, --frames FRAMES : the number of frames in the animation")
  print("    -n NODES, --nodes NODES    : the number of nodes in the simulation")
  print("    -c CXN, --connections CXN  : the number of connections in the simulation")

try:
  opts, args = getopt.getopt(
    sys.argv[1:],
    "hs:f:n:c:",
    ["help", "size=", "frames=", "nodes=", "connections="]
  )
except getopt.GetoptError as err:
  print(err)
  usage()
  sys.exit(2)

size = 400
frames = 500
nodes = 120
connections = 300
for o, a in opts:
  if o in ("-h", "--help"):
    usage()
    sys.exit()
  elif o in ("-s", "--size"):
    size = int(a)
  elif o in ("-f", "--frames"):
    frames = int(a)
  elif o in ("-n", "--nodes"):
    nodes = int(a)
  elif o in ("-c", "--connections"):
    connections = int(a)
  else:
    print("unknown option " + o)
    usage()
    sys.exit(2)

# create graph
g = Network(
  nodes=nodes,
  connections=connections,
  out_size=size
)

# animate network
operations = [
  lambda: sparkle(g, pos, SAFRON),
  lambda: sparkle(g, pos, EMERALD),
  lambda: sparkle(g, pos, PURPLE),

  lambda: g.remove_node(),
  lambda: g.remove_node(),
  lambda: g.remove_node(),

  lambda: g.add_node(),
  lambda: g.add_node(),
  lambda: g.add_node(),

  lambda: g.remove_connection(),
  lambda: g.remove_connection(),
  lambda: g.remove_connection(),
  lambda: g.remove_connection(),

  lambda: g.add_connection(),
  lambda: g.add_connection(),
  lambda: g.add_connection(),
  lambda: g.add_connection(),

  lambda: g.disable_node(),
  lambda: g.disable_node(),

  lambda: g.enable_node(),
  lambda: g.enable_node(),
]
pos = next_frame(g, arf_layout(g.graph))
for _ in range (frames):
  operations[randrange(len(operations))]()
  next_frame(g, pos)
