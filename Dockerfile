FROM ethereum/client-go:latest

RUN mkdir -p /etc/dbf /var/lib/ethereum/keystore /var/lib/ethereum/dag
COPY files/ethereum.genesys.json /etc/dbf/genesys.json
COPY files/UTC--2018-08-15T13-53-58.063479322Z--7fc4b76b8fdb6cbeab6ac52f994b2ed73114df41 /var/lib/ethereum/keystore/
RUN geth --datadir /var/lib/ethereum --networkid 28682 init /etc/dbf/genesys.json
RUN geth makedag 100 /var/lib/ethereum/dag

EXPOSE 8545

ENTRYPOINT []
CMD [ \
  "geth", \
  "--datadir", "/var/lib/ethereum", \
  "--networkid", "28682", \
  "--syncmode", "full", \
  "--rpc", "--rpcaddr", "0.0.0.0", \
  "--rpcapi", "'admin,db,eth,miner,net,personal,web3'", \
  "--rpcvhosts", "*", \
  "--maxpeers", "0", \
  "--etherbase", "7fc4b76b8fdb6cbeab6ac52f994b2ed73114df41", \
  "--ethash.dagdir", "/var/lib/ethereum/dag" \
]
