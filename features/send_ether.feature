Feature: Send Ether

  Scenario: Coinbase sends ether to me
    Given I have an Ethereum account
    And I read my balance
    And Coinbase has 0.0001 ETH
    When Coinbase sends me 0.0001 ETH
    And I wait for the transaction to be confirmed
    Then My balance has increased by 0.0001 ETH
