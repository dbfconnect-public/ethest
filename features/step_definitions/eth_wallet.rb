
Given /^I have an Ethereum account$/ do
  @accounts ||= {}
  accounts = @eth.eth_accounts
  if accounts.size > 1
    @accounts['me'] = {
      :address => accounts[1],
      :balance => 0
    }
  else
    @accounts['me'] = {
      :address => @eth.personal_new_account('fubar'),
      :balance => 0
    }
  end
  @eth.personal_unlock_account(
    @accounts['me'][:address],
    'fubar'
  )
end

Given /^I read my balance$/ do
  @accounts['me'][:balance] = Integer(
    @eth.eth_get_balance(@accounts['me'][:address])
  )
end

Given /^I have (\d+\.?\d*) ETH$/ do |amount|
  amount = @eth.to_wei(amount.to_f)

  # credit amount to coinbase
  balance = Integer(@eth.eth_get_balance($coinbase))
  if balance < amount
    @eth.mine(1 + (amount - balance).ceil.div(@eth.to_wei(3)))
  end

  # unlock coinbase
  @eth.personal_unlock_account($coinbase, 'fubar')

  # send ether
  transaction = @eth.eth_send_transaction({
    :from => $coinbase,
    :to => @accounts['me'][:address],
    :value => @eth.int_to_hex(amount)
  })

  # wait for tx confirmation
  while true
    @eth.mine()
    tx = @eth.eth_get_transaction_by_hash(transaction)
    break unless tx['blockNumber'].nil?
    sleep(0.1)
  end
end

Given /^Coinbase has (\d+\.?\d*) ETH$/ do |amount|
  amount = @eth.to_wei(amount.to_f)
  balance = Integer(@eth.eth_get_balance($coinbase))
  if balance < amount
    @eth.mine(1 + (amount - balance).ceil.div(@eth.to_wei(3)))
  end
end

When /^Coinbase sends me (\d+\.?\d*) ETH$/ do |amount|
  # unlock coinbase
  @eth.personal_unlock_account($coinbase, 'fubar')

  # send ether
  amount = @eth.to_wei(amount.to_f)
  @transaction = @eth.eth_send_transaction({
    :from => $coinbase,
    :to => @accounts['me'][:address],
    :value => @eth.int_to_hex(amount)
  })
end

When /^I wait for the transaction to be confirmed$/ do
  while true
    @eth.mine()
    tx = @eth.eth_get_transaction_by_hash(@transaction)
    break unless tx['blockNumber'].nil?
    sleep(0.1)
  end
end

Then /^My balance has increased by (\d+\.?\d*) ETH$/ do |amount|
  amount = @eth.to_wei(amount.to_f)
  balance = Integer(@eth.eth_get_balance(@accounts['me'][:address]))
  assert_equal(
    amount,
    balance - @accounts['me'][:balance],
  )
  @accounts['me'][:balance] = balance
end
