
When /^I deploy the Greater contract with "(.*)"$/ do |greetings|
  greeter = Ethereum::Contract.create(file: "files/hello.sol", client: @eth.client)
  greeter.gas_limit = 3_000_000
  deploy = greeter.deploy(greetings)
  @transaction = deploy.id
end

Then /^The Greeter greets with "(.*)"$/ do |greetings|
  # get contract
  tx = @eth.eth_get_transaction_receipt(@transaction)
  greeter = Ethereum::Contract.create(
    file: "files/hello.sol",
    address: tx['contractAddress'],
    client: @eth.client
  )

  # call and check coontract
  assert_equal(greetings, greeter.call.greet())
end
