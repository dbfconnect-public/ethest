Feature: Deploy Smart Contract

  Scenario: I deploy Hello World contract
    Given I have an Ethereum account
    And I have 0.01 ETH
    When I deploy the Greater contract with "Hello World!"
    And I wait for the transaction to be confirmed
    Then The Greeter greets with "Hello World!"
