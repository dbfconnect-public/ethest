require 'ethereum'

# Ethereum::HttpClient wrapper that strips out RPC call
# to bare 'result' attribute.
# It also exposes the Ethereum::Formatter methods.
class EthereumClient < Ethereum::Formatter
  attr_reader :client

  def initialize(host, port)
    @client = Ethereum::HttpClient.new("http://#{host}:#{port}", nil, true)
  end

  (Ethereum::Client.new.public_methods - Object.new.methods).each do |m|
    if m.match?(/^(eth|miner|personal)_/)
      define_method m do |*args|
        result = @client.send(m, *args)
        return result['result']
      end
    else
      define_method m do |*args|
        return @client.send(m, *args)
      end
    end
  end

  def mine(blk=1)
    blkcnt = Integer(self.eth_block_number)
    self.miner_start()
    while Integer(self.eth_block_number) < (blkcnt + blk.to_i)
      sleep(0.1)
    end
    self.miner_stop()
  end
end
