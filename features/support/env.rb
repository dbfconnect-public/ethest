require 'test/unit/assertions'
require 'ethereum'

World(Test::Unit::Assertions)

Before do
  host = if ENV.key?('ETHNODE_HOST')
    ENV['ETHNODE_HOST']
  else
    'localhost'
  end
  port = if ENV.key?('ETHNODE_PORT')
    ENV['ETHNODE_PORT'].to_i
  else
    8545
  end
  @eth = EthereumClient.new(host, port)
end

$coinbase = "0x7fc4b76b8fdb6cbeab6ac52f994b2ed73114df41"
