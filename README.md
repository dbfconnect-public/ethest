ethest
======
Ethereum Node for development and testing.

Run with Ansible
----------------
Start the node:
```
$ ansible-playbook ethest.yml
```

Run with Dokcer
---------------
Build the docker image:
```
$ docker build -t ethest .
```

Start the node :
```
$ docker run --name ethest --publish 8545:8545 --rm --detach ethest
```

Connect to the Node
-------------------
Using `geth`:
```
$ geth attach http://localhost:8545
```

Test the Node
-------------
Using `ruby-2.5`:
```
$ gem install bundler
$ bundle install
$ bundle exec cucumber
```

See also
--------

https://dbfconnect-public.gitlab.io/ethest/presentation/ethest.html
